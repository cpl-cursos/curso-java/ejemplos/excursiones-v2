package com.cplcursos.Excursionesv2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExcursionesV2Application {

	public static void main(String[] args) {
		SpringApplication.run(ExcursionesV2Application.class, args);
	}

}
